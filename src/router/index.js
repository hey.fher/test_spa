import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
	mode: 'history',
	
	routes: [{
		path: '/',
		name: 'home',
		component: () => import('@/views/Home')
    }, {
		path: '*',
		name: 'notfound',
		component: () => import('@/views/404')
	}],

	scrollBehavior(to, from, savedPosition) {
		return {
			x: 0,
			y: 0
		}
	}
})


export default router
