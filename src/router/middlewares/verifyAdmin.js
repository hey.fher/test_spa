import Store from './../../store'

export default (to, from, next) => {
    let role = localStorage.getItem('l') ? JSON.parse(atob(localStorage.getItem('l'))).role : undefined

    if (role === 'admin') {
        next()
    } else {
        next({ name: 'signin' })

        Store.dispatch('root/$setNotification', {
            success: false,
            error: 'No tienes autorización para ver esta página'
        })
    }
}
