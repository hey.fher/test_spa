import Vue from 'vue'

export default Vue.filter('lowercase', (payload = '') => {
    return payload.toLowerCase()
})
