import Vue from 'vue'

export default Vue.filter('smallDescription', (payload = 'Sin descripción', max = 100) => {
    return payload.length > max ? payload.substring(0, max).trimRight() + '...' : payload
})
