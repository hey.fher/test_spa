import Vue from 'vue'

export default Vue.filter('uppercase', (payload = '') => {
    return payload.toUpperCase()
})
