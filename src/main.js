import VeeValidate, { Validator } from 'vee-validate'

import axios from 'axios'
import Vue from 'vue'
import Vuex from 'vuex'
import moment from 'moment'
import App from './App.vue'

import router from './router'
import store from './store'
import filters from './filters'
import directives from './directives'



/**
 * Plugins
 */
Vue.use(VeeValidate, {
	locale: 'en'
})



/**
 * Global Instances
 */
const $http = axios.create({
	baseURL: `${ process.env.VUE_APP_SERVER }/api`,
	withCredentials: true
})

Vue.prototype.$http = $http
Vue.prototype.$moment = moment
Vue.prototype.$bus = new Vue()

Vuex.Store.prototype.$http = $http
Vuex.Store.prototype.$router = router



/**
 * Entry point
 */
new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
