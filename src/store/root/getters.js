export default {
	_getNotification(state) {
		return state.notification
	},

	_getCandidates(state) {
		return state.candidates
	}
}
