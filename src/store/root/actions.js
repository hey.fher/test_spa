export default {
	$setNotification({ commit }, data) {
		commit('SET_NOTIFICATION', data)
	},

	$getCandidates({ commit }, data)  {
		return new Promise(resolve => {
			this.$http.get('/candidate', data).then(({ data }) => {
				if (data.success) {
					commit('SET_CANDIDATES', data.payload)
				}

				resolve(data)
			})
		})
	},

	$handleCandidate({ commit }, { method, url, form, options }) {
		return new Promise(resolve => {
			this.$http[method](url, form, options).then(({ data }) => {
				if (data.success) {
					if (method === 'post') {
						commit('CREATE_CANDIDATE', data.payload)
					} else if (method === 'delete') {
						commit('DELETE_CANDIDATE', data.payload)
					} else if (method === 'put') {
						commit('UPDATE_CANDIDATE', data.payload)
					}
				}

				resolve(data)
			})
		})
	}
}
