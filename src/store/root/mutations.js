export default {
	SET_NOTIFICATION(state, payload) {
		state.notification = payload
	},

	SET_CANDIDATES(state, payload)  {
		state.candidates = payload
	},

	CREATE_CANDIDATE(state, payload) {
		state.candidates.unshift(payload)
	},

	DELETE_CANDIDATE(state, payload) {
		let candidates = state.candidates

		for (let c = 0; c < candidates.length; c++) {
			let candidate = candidates[c]

			if (candidate._id === payload._id) {
				return candidates.splice(c, 1)
			}
		}
	},

	UPDATE_CANDIDATE(state, payload)  {
		let candidates = state.candidates

		for (let c = 0; c < candidates.length; c++) {
			let candidate = candidates[c]

			if (candidate._id === payload._id) {
				return candidates.splice(c, 1, payload)
			}
		}
	}
}
