export default {
    data() {
        return {
            modalVisible: false,
            modalPayload: undefined,
            modalCallback: undefined,
        }
    },

    watch: {
        $route(value) {
            this.closeModal()
        },

        modalPayload(value) {
            if (value) {
                return this.form = JSON.parse(JSON.stringify(value))
            }

			this.$validator.pause()
			this.form = {}
			this.$nextTick(() => {
				this.$validator.resume()
			})
        }
    },

    computed: {
        isEditMode() {
            return !!this.modalPayload
        },

        humanizeMode() {
            return this.isEditMode ? 'Edit' : 'Create'
        },
    },

    methods: {
        openModal(modalPayload, modalCallback) {
            document.body.style.overflowY = 'hidden'

            if (modalPayload) {
                this.modalPayload = modalPayload
            }

            if (modalCallback) {
                this.modalCallback = modalCallback
            }

            this.modalVisible = true
        },

        closeModal(event) {
            if (event) {
                if (event.currentTarget !== event.target) {
                    return
                }
            }

            this.resetModal()
        },

        acceptModal(payload) {
            if (this.modalCallback) {
                this.modalCallback(payload)
            }

            this.resetModal()
        },

        resetModal() {

            this.modalVisible = false
            this.modalPayload = undefined
            this.modalCallback = undefined

            document.body.style.overflowY = 'auto'
        }
    },

    mounted() {
        window.addEventListener('keydown', e => {
            if (this.modalVisible && e.code === 'Escape' || e.keyCode === 27) {
                this.resetModal()
            }
        })
    }
}
