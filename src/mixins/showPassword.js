/**
 * div
 *     input
 *     icon(@click="showPassword")
 */
export default {
    methods: {
        showPassword(e) {
            let target = e.currentTarget
            let input = target.parentNode.firstChild

            if (input.type === 'password') {
                input.type = 'text'
                target.className = target.className.replace('fa-eye', 'fa-eye-slash')
                return
            }

            input.type = 'password'
            target.className = target.className.replace('fa-eye-slash', 'fa-eye')
        }
    }
}
