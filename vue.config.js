module.exports = {
    css: {
        loaderOptions: {
            sass: {
                data: `
                    @import "./src/assets/scss/core/__colors.scss";
                    @import "./src/assets/scss/core/__variables.scss";
                    @import "./src/assets/scss/core/__mixins.scss";
                `
            }
        }
    }
}
