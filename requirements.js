// [
//   {
//     "id": 1,
//     "name": "Marco",
//     "last_name": "Tzuc May",
//     "phone": "73684763274",
//     "address": "Calle 64 e # 117 - 15",
//     "laboral_references": [
//       {
//         "id": 1,
//         "company_name": "Carrefour",
//         "contact_name": "Carlos Navarro",
//         "start_at": "20/05/1990",
//         "leave_at": "21/06/1992"
//       },
//       {
//         "id": 2,
//         "company_name": "Exito",
//         "contact_name": "Carlos Navarro",
//         "start_at": "20/05/1990",
//         "leave_at": "21/06/2019"
//       }
//     ]
//   },
//   {
//     "id": 2,
//     "name": "Hegel",
//     "last_name": "Trigo",
//     "phone": "73684763174",
//     "address": "Calle 34 e # 117 - 15",
//     "laboral_references": [
//       {
//         "id": 3,
//         "company_name": "Estados financieros sas",
//         "contact_name": "Andres Navarro",
//         "start_at": "20/05/1991",
//         "leave_at": "21/06/1991"
//       },
//       {
//         "id": 4,
//         "company_name": "Caminando",
//         "contact_name": "Carlos Mariano Zapata",
//         "start_at": "20/05/2000",
//         "leave_at": "21/06/2001"
//       }
//     ]
//   }
// ]

 // one candidate has laboral references. We need this features on our application:

// 1- Candidates Index
// 2- As an user i can clic on a candidate's name and see his references.
// 3- As an user i can edit the candidate's info
// 4- Also i can edit all of his laboral references
// 5- Also I can create one o many laboral references
// 7- As a user i can delete a candidate's info. In this issue, the app have to show me an sweetalert with this message "Este candidato tiene dos referencias laborales. Estas seguro de eliminarlo?." Only if the candidate has LB
// 8- Delete laboral references
// 9- You have to use REST to do this.
// 10-You have to use Vue router

//
// Important
//
// 1- You have to use Vue.js framework
// 2- Node Version 10.14.2

// After solve that check this exercises, you can choose any technology.

1.- A palindromic number reads the same both ways. The largest palindrome made from the
product of two 2-digit numbers is 9009 = 91 × 99.
Find the largest palindrome made from the product of two 3-digit numbers.


2.- A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,

a
2 + b
2 = c
2

For example, 3
2 + 4
2 = 9 + 16 = 25 = 5
2
.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.
//
// Globalwork.txt
// Mostrando Globalwork.txt
